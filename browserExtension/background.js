// Init
let url = "http://127.0.0.1";
let port = "4242"

// Diese Browser Extension beauftragt content.js, die URL einer fremden Seite zu ermitteln.
// background.js und content.js kommunizieren mit messages:
//
document.addEventListener('DOMContentLoaded', function () {

    // Handler für browser-action button:
    chrome.browserAction.onClicked.addListener(function (tab) {

        // Send a message to the active tab:
        chrome.tabs.query({
            active: true,
            currentWindow: true
        }, function (tabs) {
            var activeTab = tabs[0];
            chrome.tabs.sendMessage(activeTab.id, {
                "message": "backgroundAnContent_hatDraufGedrueckt"
            });
        });

    });


    // Listener für Empfang von messages (hier von content.js):
    chrome.runtime.onMessage.addListener(
        function (request, sender, sendResponse) {

            if (request.message === "contentAnBackground_hierDattHref") {

                //-----------------------------------------------------------------------
                // Ajax-Request, Daten der message von content.js an container senden:
                //
                // Name/Wert-Paar definieren:
                var name = "donald.duck@stud.fh-flensburg.de";
                var wert = {
                    href: request.href
                };
                var wertJsonStr = JSON.stringify(wert);

                var containerSetUrl = url + ":" + port + "/containerSet";
				//"http://borsti.inf.fh-flensburg.de:4242/containerSet";
                var xhr1 = new XMLHttpRequest();

                // Ajax Request senden:
                xhr1.open("POST", containerSetUrl);
                // NICHT als 'application/json' senden, denn nur der Parameter wertJsonStr enthält JSON:
                xhr1.setRequestHeader("Content-type", 'application/x-www-form-urlencoded');
                xhr1.send('name=' + name + "&wertJsonStr=" + wertJsonStr);

                // Ajax-Antwort behandeln:
                xhr1.addEventListener("load", function () {

                    //-----------------------------------------------------------------------
                    // Neuen Tab mit urlNeuerTab öffnen. Falls diese Url bereits in einem Tab geöffnet ist, 
                    // diesen Tab aktivieren und neu laden:
                    var urlNeuerTab = url + ":" + port + "/*";

                    chrome.tabs.query({
                        url: urlNeuerTab
                    }, function (tabs) {
                        if (tabs[0]) {
                            // es existiert ein Tab mit dieser urlNeuerTab:
                            // -> Tab aktivieren
                            // -> Tab neu laden
                            chrome.tabs.update(tabs[0].id, {
                                active: true
                            });
                            chrome.tabs.reload(tabs[0].id);
                        } else {
                            // neuen Tab mit urlNeuerTab öffnen:
                            chrome.tabs.create({
                                url: urlNeuerTab
                            });
                        }
                    });
                });
            }
        }
    );
});