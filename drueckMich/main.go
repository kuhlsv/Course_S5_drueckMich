// Main for drückMich
// Packages db, login, mgo.v2 are needed for this project
package main

import (
	"drueckMich/controllers"
	"drueckMich/db"
	"log"
	"net/http"
)

/*
 *	Initialisation
 */
var configFile = "./conf/config.json"

/*
 *	Main function for this application
 *	Call this to start the application
 */
func main() {
	// Load config file
	//dir, e := filepath.Abs(filepath.Dir(os.Args[0]))
	var config = controllers.LoadConfiguration(configFile)
	/*
	* Common data database
	 */
	// Initialise DB-connection from config
	db.Database(config.Database.Name, config.Database.Host + ":" + config.Database.Port)
	/*
	* GridFS database
	 */
	db.Init(config.Database.Grid, config.Database.Host + ":" + config.Database.Port)
	// This deletes the db to a clean state on startup
	//disabled db.OpenConnection().DropDatabase()
	//disabled db.OpenCon().DropDatabase()
	/*
	* Handler
	 */
	http.Handle("/images/", http.FileServer(http.Dir("./views/")))
	http.Handle("/assets/", http.FileServer(http.Dir("./views/")))
	http.HandleFunc("/", controllers.CheckLogin)
	http.HandleFunc("/login", controllers.ShowLogin)
	http.HandleFunc("/categories", controllers.InterfaceHandler)
	http.HandleFunc("/bookmarks", controllers.InterfaceHandler)
	http.HandleFunc("/profile", controllers.InterfaceHandler)
	http.HandleFunc("/impressum", controllers.InterfaceHandler)
	http.HandleFunc("/about", controllers.InterfaceHandler)
	http.HandleFunc("/policy", controllers.InterfaceHandler)
	http.HandleFunc("/user", controllers.UserHandler)
	http.HandleFunc("/data", controllers.AjaxHandler)
	http.HandleFunc("/containerSet", controllers.AjaxHandler)
	http.HandleFunc("/getGridImage", controllers.GridImagePageHandler)
	err := http.ListenAndServe(":"+config.Web.Port, nil)
	if err != nil {
		log.Print(err.Error())
	}
}