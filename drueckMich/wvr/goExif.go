// decodeExif.go aus:
// https://github.com/rwcarlsen/goexif
// installieren mit ---> go get github.com/rwcarlsen/goexif/exif

package wvr

import (
	"fmt"
	"github.com/rwcarlsen/goexif/exif"
	"log"
	"net/url"
	"os"
)

func getCoordinatesFromIamge(imageUrl *url.URL) {
	fname := "./IMG_20180720_095758.jpg"
	//	fname := "./Eiffelturm.JPG" // enthält keine GPS-Daten-> Decode error
	f, err := os.Open(fname)
	if err != nil {
		fmt.Println("File can not opened:")
		log.Fatal(err)
	}
	x, err := exif.Decode(f)
	if err != nil {
		fmt.Println("No exif-data found:")
		log.Fatal(err)
	}
	latitude, longitude, _ := x.LatLong()
	width, _ := x.Get("ImageWidth")
	height, _ := x.Get("ImageLength")
	fmt.Println("Lat: ", latitude)
	fmt.Println("Long: ", longitude)
	fmt.Println("\nBreite des Bildes: ", width)
	fmt.Println("Höhe des Bildes: ", height)
	
}
