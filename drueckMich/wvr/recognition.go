// siehe:
// https://www.ibm.com/watson/services/visual-recognition/
// https://github.com/watson-developer-cloud/go-sdk
// https://github.com/watson-developer-cloud/go-sdk/blob/master/examples/visualrecognitionv3/visual_recognition_v3.go
// Install: go get -u github.com/watson-developer-cloud/go-sdk/...
// 			go get github.com/go-openapi/strfmt

package wvr

import (
	"github.com/watson-developer-cloud/go-sdk/core"
	"github.com/watson-developer-cloud/go-sdk/visualrecognitionv3"
	"log"
	"sort"
)

// Represent classification data
type Classification struct{
	Class  string
	Score  float32
}

// @params: images as string slice and a1pi key like -> "npHQK2SFecYwLpmVijx1JL8Yxua05vC9Pt7LfbI-x1wG"
func CheckImages(images []string, apiKey string) []string{
	// Watson Visual Recognition Service instanziieren:
	service, serviceErr := visualrecognitionv3.
		NewVisualRecognitionV3(&visualrecognitionv3.VisualRecognitionV3Options{
			URL:       "https://gateway.watsonplatform.net/visual-recognition/api",
			Version:   "2018-03-19",
			IAMApiKey: apiKey,
		})
	if serviceErr != nil {
		panic(serviceErr)
	}
	// Ausgabesprache definieren:
	sprache := new(string)
	*sprache = "de"

	var classes []Classification
	for _, image := range images{
		// Image klassifizieren:
		//--------------------------------------------------------------------------
		// A) Image per URL definieren:
		//--------------------------------------------------------------------------
		// Optionen für die Klassifizierung festlegen:
		classifyOptions := service.NewClassifyOptions()
		classifyOptions.URL = core.StringPtr(image)
		// Schwellwert für den "Verlässlichkeitsscore":
		classifyOptions.Threshold = core.Float32Ptr(0.6)
		classifyOptions.ClassifierIds = []string{"default"}
		classifyOptions.AcceptLanguage = sprache
		response, responseErr := service.Classify(classifyOptions)
		// Ergebnisdaten aufbereiten:
		classifyResult := service.GetClassifyResult(response)
		if responseErr != nil || classifyResult == nil {
			// responseErr: Images that can not be read
			log.Print("\n")
			//log.Print("Can not get an image for analysis\n") // + responseErr.Error()
		}else{
			//core.PrettyPrint(classifyResult, "---JSON output")
			class := classifyResult.Images[0].Classifiers[0].Classes
			for _, wert := range class {
				buffer := Classification{}
				buffer.Class = *wert.ClassName
				buffer.Score = *wert.Score
				classes = append(classes, buffer)
			}
		}
	}

	// Sort by score and return as one slice
	var result []string
	sort.Slice(classes, func(i, j int) bool {
		return classes[i].Score > classes[j].Score
	})
	for _, item := range classes{
		result = append(result, item.Class)
	}
	//fmt.Print("-------------------------\n")
	//fmt.Print(result)
	//fmt.Print("\n-------------------------\n")
	return result
}
