// Class for help with one-line if statements
package helpers

/*
*
*	IfElse one-liner helper
*
 */
func IfThenElse(condition bool, a interface{}, b interface{}) interface{} {
	if condition {
		return a
	}
	return b
}

func IfThenElseString(condition bool, a string, b string) string {
	if condition {
		return a
	}
	return b
}

func IfThenElseInt(condition bool, a int, b int) int {
	if condition {
		return a
	}
	return b
}