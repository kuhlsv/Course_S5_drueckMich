// Class for web requests and html scraping
// Dieses Programm
//  - liest als HTTP-Client eine HTML-Seite (pageUrl) ein
//  - wandelt die Seite in einen html-doc tree
//  - schreibt die src-Attributwerte (URLs) aller Images dieser Seite in eine Map
//  - gibt diese (meist relativen) URLs aus
//  - konvertiert alle URLs in absolute URLs und gibt auch sie aus

package web

import (
	"golang.org/x/net/html"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
)

/*
 *	Initialisation
 */
var srcAttributes = make(map[int]string)
var attributes = make(map[int]string)
var pageUrl string

// A ScraperData represents all data get from webscraper
type ScraperData struct{
	Images           []string
	Icon 			 string
	MetaDesc         []string
	Title			 string
}

/*
 *	Function to set the page to call
 *
 *	@params: String -> The page url
 */
func SetPage(url string){
	pageUrl = url
}

/*
 *	Search recursive for all img-nodes and write all src-attributes into the map
 *  Code by Tepper
 *
 *	@params: Node -> The next html node
 */
func searchForSrcAttributes(node *html.Node) {
	if node.Type == html.ElementNode && node.Data == "img" {
		for _, img := range node.Attr {
			if img.Key == "src" {
				// mit nächstem int-Index auf Map pushen:
				if len(srcAttributes) == 0 {
					srcAttributes[0] = img.Val
				} else {
					srcAttributes[len(srcAttributes)] = img.Val
				}
				break
			}
		}
	}
	for child := node.FirstChild; child != nil; child = child.NextSibling {
		searchForSrcAttributes(child)
	}
}

/*
 *	Search recursive for descriptions and write into the map
 *
 *	@params: Node -> The next html node
 */
func searchForDescriptions(node *html.Node) {
	check := false
	if node.Type == html.ElementNode && node.Data == "meta" {
		for _, name := range node.Attr {
			if check {
				// mit nächstem int-Index auf Map pushen:
				if len(attributes) == 0 {
					attributes[0] = name.Val
				} else {
					attributes[len(attributes)] = name.Val
				}
				check = false
				break
			}
			if name.Val == "description" || name.Val == "Description" {
				check = true
			}
		}
	}
	for child := node.FirstChild; child != nil; child = child.NextSibling {
		searchForDescriptions(child)
	}
}

/*
 *	Search recursive for icons and write into the map
 *
 *	@params: Node -> The next html node
 */
func searchForIcons(node *html.Node) {
	check := false
	if node.Type == html.ElementNode && node.Data == "link" {
		for _, name := range node.Attr {
			if check {
				// mit nächstem int-Index auf Map pushen:
				if name.Key == "href" {
					if len(attributes) == 0 {
						attributes[0] = name.Val
					} else {
						attributes[len(attributes)] = name.Val
					}
					check = false
					break
				}
			}
			if name.Val == "image/x-icon" || name.Val == "shortcut icon" {
				check = true
			}
		}
	}
	for child := node.FirstChild; child != nil; child = child.NextSibling {
		searchForIcons(child)
	}
}

/*
 *	Search recursive for specific named-nodes and write content into the map
 *
 *	@params: String, Node -> The node to collect and the next html node
 */
func searchForNode(nodeName string, node *html.Node) {
	if node.Type == html.ElementNode && node.Data == nodeName {
		attributes[0] =  node.FirstChild.Data
	}
	for child := node.FirstChild; child != nil; child = child.NextSibling {
		searchForNode(nodeName, child)
	}
}

/*
 *	This function run the scrapping execution
 *	Call this to start the scraper
 */
func Scrap(pageAdress string) ScraperData {
	SetPage(pageAdress)
	data := ScraperData{}
	// Get images
	// Use slice instead of list
	// Lists in go not used like python
	//var imagesSlice []*url.URL = make([]*url.URL,5,10)
	if len(pageUrl) > 0 {
		/*
		* Start Code by Tepper
		 */
		// HTTP-GET Request senden:
		res, err := http.Get(pageUrl)
		if err != nil {
			log.Fatal(err)
		}
		byteArrayPage, err := ioutil.ReadAll(res.Body)
		res.Body.Close()
		if err != nil {
			log.Fatal(err)
		}
		// Empfangene Seite parsen, in doc-tree wandeln:
		docZeiger, err := html.Parse(strings.NewReader(string(byteArrayPage)))
		if err != nil {
			log.Fatal(err)
		}
		searchForSrcAttributes(docZeiger)
		// Alle relativen SRC-URLs in absolute URLs wandeln:
		// https://golang.org/pkg/net/url/#example_URL_Parse
		// Zunächst die pageUrl (raw-url) in eine URL-structure wandeln:
		u, err := url.Parse(pageUrl)
		if err != nil {
			log.Fatal(err)
		}
		// Nun alle URLs aus der Map im Kontext der pageUrl u in
		// absolute URLS konvertieren:
		for _, wert := range srcAttributes {
			absURL, err := u.Parse(wert)
			if err != nil {
				log.Fatal(err)
			}else{
				//imagesSlice = append(imagesSlice, absURL)
				data.Images = append(data.Images, absURL.String())
			}
		}
		/*
		*  END Code by Tepper
		 */
		 // Title
		 searchForNode("title", docZeiger)
		 data.Title = attributes[0]
		 attributes = make(map[int]string)
		 // Meta
		 searchForDescriptions(docZeiger)
		 for _, wert := range attributes {
			if err != nil {
				log.Fatal(err)
			}else{
				data.MetaDesc = append(data.MetaDesc, wert)
			}
		 }
		 attributes = make(map[int]string)
		 // Icon
		 searchForIcons(docZeiger)
		 absURL, err := u.Parse(attributes[0])
		 if err != nil {
			log.Fatal(err)
		 }
		 data.Icon = absURL.String()
	}
	return data
}
