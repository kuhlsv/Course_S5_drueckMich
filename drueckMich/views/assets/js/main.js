"use strict";
///////////////////////////////////////////////////////////////////////////////////////
///// General
///////////////////////////////////////////////////////////////////////////////////////

/*
    Init
*/
window.addEventListener("load", function(){ init() });

/*
    Helper
*/
let ajaxInterval = 1000;
let popupTime = 3000;
let lock = false;
// Create host from config
let host = 'web://localhost:4242';

function $id(id){
    return document.getElementById(id);
}
function $tag(tag){
    return document.getElementsByTagName(tag);
}
function $name(name){
    return document.getElementsByName(name);
}

// Start function
function init() {
    // EventHandler
    bookmarksListener();
    // Check Filter/Sort
    checkCustomization();
    // Popup
    manualPopup();
}

////
// Function to decode names and ids
// Params: text -> text to be converted
// Return: converted text
////
function decodeHTMLEntities(text) {
    let entities = [
        ['amp', '&'], ['apos', '\''], ['#x27', '\''],
        ['#x2F', '/'], ['#39', '\''], ['#47', '/'],
        ['lt', '<'], ['gt', '>'], ['nbsp', ' '], ['quot', '"']
    ];
    for (let i = 0, max = entities.length; i < max; ++i)
        text = text.replace(new RegExp('&'+entities[i][0]+';', 'g'), entities[i][1]);
    return text;
}

// Reload page with new GET params
function reloadWithGet(key, value) {
    key = encodeURI(key);
    value = encodeURI(value);
    let url = window.location.href;
    let kvp = document.location.search.substr(1).split('&');
    let i=kvp.length;
    let x;
    while(i--){
        x = kvp[i].split('=');
        if (x[0]==key){
            x[1] = value;
            kvp[i] = x.join('=');
            break;
        }
    }
    if(i<0){
        kvp[kvp.length] = [key,value].join('=');
    }
    document.location.search = kvp.join('&');
}

// Search for GET params
function findGetParameter(parameterName) {
    let result = null,
        tmp = [];
    let items = location.search.substr(1).split("&");
    for (let index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] === parameterName) {
            result = decodeURIComponent(tmp[1]);
        }
    }
    return result;
}

function messageDialog() {
    let m = $id("message");
    m.style.display = "inline-block";
    // Reset user message
    let url = host.address + host.port + "/data?reset=true";
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.send();
    // Hide message popup
    window.setTimeout(function () {
        m.style.display = "none";
    }, popupTime);
}

function manualPopup() {
    let tooltip = $id("manual");
    if(tooltip){
        setTimeout(function () {
            tooltip.style.display = "none";
        },5000);
    }
}

///////////////////////////////////////////////////////////////////////////////////////
///// Bookmarks
///////////////////////////////////////////////////////////////////////////////////////

// Function to load event listeners for bookmarks
function bookmarksListener() {
    let sort = $id("sort");
    let filter = $id("filter");
    if (sort){
        sort.addEventListener("change", function(){
            reloadWithGet("sort",sort.value);
        });
        filter.addEventListener("change", function(){
            if (filter.value === "") {
                window.location.href =  window.location.href.split("?")[0];
            }else{
                reloadWithGet("filter",filter.value);
            }
        });
    }
}

// Function to count up clicks on Bookmark
function count(bm) {
    let data = "?count="+bm;
    let url = "/bookmarks" + data; // host +
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.send();
    window.location.reload(false);
}

// Function to count up clicks on Bookmark
function del(bm) {
    let data = "?del="+bm;
    let url = "/bookmarks" + data; // host +
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.send();
    window.location.reload(false);
}

// Function to load "editor" for title
// On focus => Create input to edit
// On focus lost => Remove input und send request
function editCell(cell, item, id) {
    let timeoutid = 0;
    let content = cell.textContent;
    let input = document.createElement("input");
    input.value = content;
    let list = $id(item);
    if (list){
        // add datalist
        input.setAttribute('list',item);
    }
    if(!lock){
        lock = true;
        cell.textContent = "";
        input.addEventListener("focusout", function(){
            if(timeoutid === 0){
                timeoutid = setInterval(function () {
                    if(!(input === document.activeElement)){
                        event(item, id);
                        lock = false;
                        clearInterval(timeoutid);
                        timeoutid = 0;
                    }
                },500)
            }
        });
        cell.append(input);
        input.focus();
    }
    function event(i, id){
        content = input.value;
        cell.removeChild(cell.firstChild);
        cell.textContent = content;
        // ajax
        let data = "?update="+i+"&id="+id+"&content="+content;
        let url = "/bookmarks" + data;
        let xhr = new XMLHttpRequest();
        xhr.open("GET", url);
        xhr.send();
        xhr.addEventListener("load", function(){
            console.log("Updated " + i + ": " + content + " for " + id)
        })
    }
}

// Check customisation view by filter and sort
// Update the inputs
function checkCustomization(){
    let sortGET = findGetParameter("sort");
    let filterGET = findGetParameter("filter");
    let sort = $id("sort");
    let filter = $id("filter");
    if(sortGET){
        sort.value = sortGET;
    }
    if(filterGET){
        filter.value = filterGET;
    }
}

///////////////////////////////////////////////////////////////////////////////////////
///// Categories
///////////////////////////////////////////////////////////////////////////////////////

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev, man, auto) {
    ev.preventDefault();
    let data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
    // Change category
    let val = man !== "" ? man : auto;
    let id = document.getElementById(data).id;
    let idSplit = id.split('\"')[1];
    // ajax
    let dataGet = "?update=categories&id="+idSplit+"&content="+val;
    let url = "/bookmarks" + dataGet;
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.send();
    xhr.addEventListener("load", function(){
        console.log("Updated categorie: " + val + " for " + idSplit)
    })
}