// Package for a login
// Call the CheckLogin function
// The construction of HTML forms and inputs is important for this (variables)
// Enable 'password hash' (method register) after testing to implement security
package login

import (
	"drueckMich/db"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"math/rand"
	"net/http"
	"strconv"
	"sync"
)

/*
 *	Initialisation
 */
const DB_COLL_NAME = "Userdata"
// Username and password length for registration
const UNAME_LENGHT = 3
const PWORD_LENGTH = 2
// Define the cookie name for sessions
const COOKIE_NAME = "session"
// Post value names
const BUTTON_NAME = "authBtn"
const UNAME_NAME = "uname"
const PWORD_NAME = "passwd"
// Define the name of the buttons for auth
// This obj get values from them so make sure they are correct
var formButtons = []string  { "Login", "Registration", "Logout" }
// Mutual exclusion lock to handle ReadWrite access by multiple threads
var storageMutex sync.RWMutex
// Info message returned by calling functions
var Message string

// An User represents a user with basic data
type User struct {
	Id 	 bson.ObjectId 	`bson:"_id"`
	Username string 	`bson:"Username"`
	Title string 		`bson:"Title"`
	Password string 	`bson:"Password"`
	SessionId string 	`bson:"Session"`
	Theme int 			`bson:"Theme"`
	Avatar string 		`bson:"Avatar"`
}

// Interface to implement login
type ILogin interface {
	CheckLogin(w http.ResponseWriter, r *http.Request) bool
	CheckCookie(w http.ResponseWriter, r *http.Request) bool
	DeleteCookie(w http.ResponseWriter)
}

/*
 *	Basic function for this package
 *
 *	@params: ResponseWriter, Request -> For cookie handling
 *	@return: Boolean -> Action was valid
 */
func CheckLogin(w http.ResponseWriter, r *http.Request) bool{
	var valid bool
	database := db.GetConnection()
	coll := database.C(DB_COLL_NAME)
	// Get form data
	button := r.PostFormValue(BUTTON_NAME) // r.Form[]
	if len(button) <= 2 {
		// Check for cookies to handle session, because no action found
		valid = CheckCookie(w, r)
	}else{
		uname := r.PostFormValue(UNAME_NAME)
		password := r.PostFormValue(PWORD_NAME)
		switch button {
		case formButtons[0]:
			// Call login
			valid = login(w, r, uname, password, coll)
		case formButtons[1]:
			// Call registration
			valid = register(uname, password, coll)
		case formButtons[2]:
			// Call logout
			logout(w, uname, coll)
			http.Redirect(w, r, r.Header.Get("Host") + "/login", 301)
			valid = false
		default:
			valid = false
			Message = "Error: Unknown"
		}
	}
	return valid
}

/*
 *	Basic function for sessions
 *
 *	@params: ResponseWriter, Request -> For cookie handling
 *	@return: Boolean -> Cookie is valid
 */
func CheckCookie(w http.ResponseWriter, r *http.Request) bool{
	var valid bool
	cookie, err := r.Cookie(COOKIE_NAME)
	database := db.GetConnection()
	coll := database.C(DB_COLL_NAME)
	if err != nil {
		if err != http.ErrNoCookie {
			fmt.Fprint(w, err.Error())
			valid = false
			Message = "Error: No Session"
		} else {
			err = nil
		}
	}
	if cookie != nil {
		// Check session is valid
		result := User{}
		storageMutex.RLock()
		coll.Find(bson.M{ "Session" : cookie.Value }).One(&result)
		storageMutex.RUnlock()
		if result.SessionId != "" {
			valid = true
			Message = ""
		}
	}else{
		valid = false
	}
	return valid
}

/*
 *	Function to handle registration
 *
 *	@params: Username(String), Password(String), Collection -> Get username and password
 *	@return: Boolean -> Action was valid
 */
func register(uname string, password string, coll *mgo.Collection) bool{
	var valid bool
	// Check if the input of username and password is valid
	if validatePassword(password) && validateUsername(uname) {
		// Insert to database
		coll.Insert(User{
			bson.NewObjectId(),
			uname,
			"",
			password, // hash(password) | disabled security
			"",
			0,
			"",
		})
		Message = "Rigistered"
		valid = false
	}else{
		valid = false
		if Message == "" {
			Message = "Successfully registered"
		}
	}
	return valid
}

/*
 *	Function to handle login
 *
 *	@params: ResponseWriter, Request, Username(String), Password(String), Collection -> Get cookie, username and password
 *	@return: Boolean -> Action was valid
 */
func login(w http.ResponseWriter, r *http.Request, uname string, password string, coll *mgo.Collection) bool{
	var valid bool
	result := User{}
	coll.Find(bson.M{ "Username" : uname }).One(&result)
	// Check if username and password from input is valid with db
	if result.Username == uname && result.Password == password { // hash(password) | disabled security
		// Create session
		sessionId := generateSessionId()
		coll.Update(result, bson.M{"$set": bson.M{ "Session" : sessionId }})
		DeleteCookie(w)
		setCookie(w, r, sessionId, coll)
		valid = true
		Message = "Successfully logged in"
	}else{
		valid = false
		if len(Message) <= 2 {
			Message = "Error: Invalid username or password"
		}
	}
	return valid
}

/*
 *	Function to handle logout
 *
 *	@params: ResponseWriter, Username(String), Collection -> Get cookie and username
 */
func logout(w http.ResponseWriter, uname string, coll *mgo.Collection){
	// Delete the session and cookie
	result := User{}
	coll.Find(bson.M{ "Username" : uname }).One(&result)
	coll.Update(result, bson.M{ "$set": bson.M{ "Session" : "" }})
	DeleteCookie(w)
	Message = "Logged out"
}

/*
 *	Helper-Function to delete an cookie of w
 *
 *	@params: ResponseWriter -> Get cookie
 */
func DeleteCookie(w http.ResponseWriter)  {
	newCookie := http.Cookie{
		Name:   COOKIE_NAME,
		MaxAge: -1,
	}
	http.SetCookie(w, &newCookie)
}

/*
 *	Function to set a new cookie to user
 *
 *	@params: ResponseWriter, Request, session id(String), Collection -> Get cookie and session
 */
func setCookie(w http.ResponseWriter, r *http.Request, sessionId string, coll *mgo.Collection) {
	// Check for present cookie
	cookie, err := r.Cookie(COOKIE_NAME)
	if err != nil {
		if err != http.ErrNoCookie {
			fmt.Fprint(w, err.Error())
			return
		} else {
			err = nil
		}
	}
	// Generate a new session
	if sessionId == "" {
		sessionId = generateSessionId()
	}
	// Set cookie to user and db
	cookie = &http.Cookie{
		Name:  COOKIE_NAME,
		Value: sessionId,
	}
	result := User{}
	storageMutex.Lock()
	coll.Find(bson.M{ "Session" : cookie.Value }).One(&result)
	coll.Update(result, bson.M{"$set": bson.M{ "Session" : sessionId }})
	storageMutex.Unlock()
	http.SetCookie(w, cookie)
}

/*
 *	Helper-Function to generate an session
 *
 *	@return: String -> Session id
 */
func generateSessionId() string{
	buffer := make([]byte, 32)
	// Random byte
	_, err := rand.Read(buffer)
	if err != nil {
		panic(err.Error())
	}
	// Encode byte
	return hex.EncodeToString(buffer)
}

/*
 *	Helper-Function to check password by rules
 *
 *	@param: String -> Password
 *	@return: Boolean -> valid
 */
func validatePassword(password string) bool{
	var valid bool
	// Length
	if len(password) >= PWORD_LENGTH {
		valid = true
	}else{
		valid = false
		Message = "Error: Invalid password length (min. " + strconv.Itoa(PWORD_LENGTH) + ")"
	}
	return valid
}

/*
 *	Helper-Function to check username by rules
 *
 *	@param: String -> Username
 *	@return: Boolean -> valid
 */
func validateUsername(uname string) bool{
	var valid bool
	// Length
	if len(uname) >= UNAME_LENGHT {
		// Check username forgiven
		database := db.GetConnection()
		coll := database.C(DB_COLL_NAME)
		result := User{}
		coll.Find(bson.M{ "Username" : uname }).One(&result)
		if result.Username != uname {
			valid = true
		}else{
			valid = false
			Message = "Error: User already exist"
		}
	}else{
		valid = false
		Message = "Error: Invalid username length (min. " + strconv.Itoa(UNAME_LENGHT) + ")"
	}
	return valid
}

/*
 *	HHelper-Function to hash an password for security
 *
 *	@param: String -> Clean password
 *	@return: String -> Hashed string
 */
func hash(data string) string{
	return base64.StdEncoding.EncodeToString([]byte(data))
}