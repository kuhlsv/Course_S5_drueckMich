// geospatialQueries.go

// Demo der MongoDB-geospatial-queries mit dem mgo-API.

// (MongoDb mit mongoDb-Compass oder robo3T observieren)

// Geospatial-Shell Beispiel aus: https://docs.mongodb.com/manual/geospatial-queries/#example
// umgesetzt mit neuem Mongo-API globalsign/mgo und MongoDb 4.0.1

// siehe auch:
// https://gist.github.com/icchan/bd42095afd8305594778
// http://icchan.github.io/2014/10/18/geospatial-querying-with-go-and-mongodb/
// https://stackoverflow.com/questions/45389410/how-to-create-hashed-index-by-mgo-golang
// https://godoc.org/labix.org/v2/mgo#Collection.EnsureIndex

package db

import (
	"encoding/json"
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var geoDB string = "geoDB"
var placesCol string = "placesCol"

func main() {

	type GeoJsonTy struct {
		Type        string    `json:"-"`
		Coordinates []float64 `json:"coordinates"`
	}
	type OrtTy struct {
		Name     string    `bson:"name" json:"name"`
		Location GeoJsonTy `bson:"location" json:"location"`
		Category string    `bson:"category"`
	}

	// DB-Session:
	dbSession, _ := mgo.Dial("localhost")
	defer dbSession.Close()
	db := dbSession.DB(geoDB)
	placesCol := db.C(placesCol)

	// Orte einfügen:
	placesCol.Insert(OrtTy{Name: "Central Park", Category: "Parks", Location: GeoJsonTy{Type: "Point", Coordinates: []float64{-73.97, 40.77}}})               // OHNE Id, -> wird generiert
	placesCol.Insert(OrtTy{Name: "Sara D. Roosevelt Park", Category: "Parks", Location: GeoJsonTy{Type: "Point", Coordinates: []float64{-73.9928, 40.7193}}}) // OHNE Id, -> wird generiert
	placesCol.Insert(OrtTy{Name: "Polo Grounds", Category: "Stadiums", Location: GeoJsonTy{Type: "Point", Coordinates: []float64{-73.9375, 40.8303}}})        // OHNE Id, -> wird generiert

	// ...createIndex( { location: "2dsphere" } ) existiert nicht im golang API, daher mit EnsureIndex:
	index := mgo.Index{
		Key: []string{"$2dsphere:location"},
		//		Bits: 26,
	}
	err := placesCol.EnsureIndex(index)
	if err != nil {
		panic(err)
	}

	// The following query uses the $near operator to return documents that are at least
	// 5000 meters from and at most 7000 meters from the specified GeoJSON point, sorted in order from nearest to farthest:
	var results []OrtTy // to hold the results

	posLongitude := -73.9667
	posLatitude := 40.78

	err = placesCol.Find(bson.M{
		"location": bson.M{
			"$near": bson.M{
				"$geometry": bson.M{
					"type":        "Point",
					"coordinates": []float64{posLongitude, posLatitude},
				},
				"$minDistance": 5000,
				"$maxDistance": 7000,
			},
		},
	}).All(&results)
	if err != nil {
		panic(err)
	}

	// convert it to JSON so it can be displayed
	formatter := json.MarshalIndent
	response, err := formatter(results, " ", "   ")

	fmt.Println(string(response))

	// Collection löschen:
	//	_ = placesCol.DropCollection()
}
