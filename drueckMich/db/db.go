// Package for a database connection
// Call the Database function at start once
// Call OpenConnection or GetConnection to get the database pointer
package db

import (
	"fmt"
	"gopkg.in/mgo.v2"
)

/*
 *	Initialisation
 */
var url = ""
var name = ""
var db *mgo.Database = nil

// Interface to implement database
type IDB interface {
	Database(dbName string, dbUrl string) // init
	OpenConnection() *mgo.Database // open and get
	GetConnection() *mgo.Database
	CloseConnection()
}

/*
 *	Basic function for this package to initialise the connection
 * 	Call once at program start
 *
 *	@params: String, String -> DB Name and DB URL:Port
 */
func Database(dbName string, dbUrl string){
	setDB(dbName)
	setUrl(dbUrl)
}

/*
 *	Function to open an connection from the db
 *
 *	@return: *Database -> Pointer of DB connection
 */
func OpenConnection() *mgo.Database{
	// Open
	session, err := mgo.Dial(url)
	if err != nil {
		fmt.Println(err)
	}
	if db == nil {
		db = session.DB(name)
	}
	//defer
	return db
}

/*
 *	Function to close an connection from the db
 */
func CloseConnection(){
	if db != nil {
		db.Session.Close()
	}
	db = nil
}

/*
 *	Function to get the connection
 *
 *	@return: *Database -> Pointer of DB connection
 */
func GetConnection() *mgo.Database{
	return db
}

/*
 *	Helper-Function to set name
 *
 *	@params: String -> DB Name
 */
func setDB(dbName string){
	name = dbName
}

/*
 *	Helper-Function to set url
 *
 *	@params: String -> DB  url
 */
func setUrl(dbUrl string){
	url = dbUrl
}