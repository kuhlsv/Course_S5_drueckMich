// Package for a database connection with gridfs
// Call the Database function at start once
// Call OpenConnection or GetConnection to get the database pointer
package db

import (
"fmt"
"gopkg.in/mgo.v2"
)

/*
 *	Initialisation
 */
var gridUrl = ""
var gridName = ""
var gridDb *mgo.Database = nil

/*
 *	Basic function for this package to initialise the connection
 * 	Call once at program start
 *
 *	@params: String, String -> DB Name and DB URL:Port
 */
func Init(dbName string, dbUrl string){
	setGridDB(dbName)
	setGridUrl(dbUrl)
}

/*
 *	Function to open an connection from the db
 *
 *	@return: *Database -> Pointer of DB connection
 */
func OpenCon() *mgo.Database{
	// Open
	session, err := mgo.Dial(gridUrl)
	if err != nil {
		fmt.Println(err)
	}
	if gridDb == nil{
		gridDb = session.DB(gridName)
	}
	//defer
	return gridDb
}

/*
 *	Function to close an connection from the db
 */
func CloseCon(){
	if gridDb != nil {
		gridDb.Session.Close()
	}
	gridDb = nil
}

/*
 *	Function to get the connection
 *
 *	@return: *Database -> Pointer of DB connection
 */
func Get() *mgo.Database{
	return gridDb
}

/*
 *	Helper-Function to set name
 *
 *	@params: String -> DB Name
 */
func setGridDB(dbName string){
	gridName = dbName
}

/*
 *	Helper-Function to set url
 *
 *	@params: String -> DB  url
 */
func setGridUrl(dbUrl string){
	gridUrl = dbUrl
}