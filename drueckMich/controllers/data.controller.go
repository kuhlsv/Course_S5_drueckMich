package controllers

import (
	"drueckMich/login"
	"drueckMich/models"
	"encoding/json"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"io"
	"log"
	"net/http"
	"strings"
	"time"
)

/*
 *	Initialisation
 */
// An Address represents a simple link
type Address struct{
	Link 	string		`json:"href"`
}

/*
 *	Function to handle ajax requests like adding new bookmarks
 *
 *	@params: ResponseWriter, Request -> For execution
 */
func AjaxHandler(w http.ResponseWriter, r *http.Request){
	//
	// Export / Import DB
	action := r.FormValue("action")
	if action != "" {
		if action == "import" {
			models.Import(w, r)
		} else if action == "export" {
			models.Export(w, r)
		}
		// Return to the login
		http.Redirect(w, r, r.Header.Get("Host") + "/login", 301)
	}
	//
	// Add Bookmarks from Browser extension
	var updateBookmarkName = GetConfig().Extension.Name
	var updateBookmarkValuePointer = GetConfig().Extension.ValueIn
	name := r.FormValue("name")
	if name == updateBookmarkName {
		var data Address
		jsonData := r.FormValue(updateBookmarkValuePointer)
		err := json.Unmarshal([]byte(jsonData), &data)
		if err == nil{
			var uid = models.GetUserData(r).Id
			if uid != "" {
				fmt.Print(data.Link + "\n")
				// Create
				bmid := models.BuildBookmark(uid, data.Link)
				// Write user info
				models.UserMessage = "Added new bookmark!"
				// Get additional data from bm-controller
				// This as goroutine in a new thread for parallel execution
				// So the server does not need to wait for this additional data
				go func(id bson.ObjectId) {
					getAdditionalBookmarkData(id)
				}(bmid)
				// Wait 100-200ms with response to get icon image
				time.Sleep(200 * time.Millisecond)
			} else {
				models.UserMessage = "Could't add a bookmark. No user found!"
			}
		}
	}
	//
	// Reset user message for frontend
	reset := r.FormValue("reset")
	if reset == "true" {
		models.UserMessage = ""
		login.Message = ""
	}
}

/*
 *	Function to handle actions for user data via ajax
 *
 *	@params: ResponseWriter, Request -> For execution
 */
func UserHandler(w http.ResponseWriter, r *http.Request){
	// Get action from button
	set := r.FormValue("set")
	del := r.FormValue("del")
	if set != "" {
		if set == "avatar" {
			models.UploadAvatar(w, r)
		}else if set == "bookmarks" {
			models.UploadBookmarks(w, r)
		}else if set == "title" {
			models.SetTitle(r, r.FormValue("title"))
		} else if set == "username" {
			models.UpdateUsername(r, r.FormValue("username"))
		}else if set == "theme" {
			models.SetTheme(r, r.FormValue("theme"))
		}
	}else if del == "user" {
		models.DeleteUser(w, r)
	}
	http.Redirect(w, r, r.Header.Get("Host") + "/profile", 301)
}

/*
 *	Request Grid image from server db
 *
 *	@params: ResponseWriter, Request -> For execution
 */
func GridImagePageHandler(w http.ResponseWriter, r *http.Request) {
	r_fileName := r.URL.Query().Get("name")
	gridFile := models.GetGridIconByName(r_fileName)
	mimeType := GetMimeType(r_fileName)
	w.Header().Add("Content-Type", mimeType)
	var err error
	if gridFile != nil {
		_, err = io.Copy(w, gridFile)
		err = gridFile.Close()
	}
	if err != nil {
		log.Print("Can not send image!")
	}
}

func GetMimeType(link string) string{
	tmpSlice := strings.Split(link, ".")
	fileExtension := tmpSlice[len(tmpSlice)-1] // last element
	fileExtension = strings.ToLower(fileExtension)
	var mimeType string
	switch fileExtension {
	case "jpeg", "jpg":
		mimeType = "image/jpeg"
	case "png":
		mimeType = "image/png"
	case "gif":
		mimeType = "image/gif"
	case "ico":
		mimeType = "image/x-icon"
	default:
		mimeType = "text/html"
	}
	return mimeType
}