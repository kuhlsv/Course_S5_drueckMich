// Class for handling interface actions
// This class check und serialize actions for the interface
// Package db, login, models is needed
package controllers

import (
	"drueckMich/db"
	"drueckMich/login"
	"drueckMich/models"
	"gopkg.in/mgo.v2/bson"
	"html/template"
	"net/http"
	"sort"
	"strings"
)

/*
 *	Initialisation
 */
var URL = GetConfig().Web.Host + ":" + GetConfig().Web.Port + "/"
const LANDING_PAGE = "bookmarks"

// Represents user data
type UserData struct{
	Message 	string
	User 		models.UserData
}
// Represent user data
type BookmarkData struct{
	Bookmarks	[]models.Bookmark
	Categories  []string
	User		models.UserData
}

/*
 *	Function to show the called page
 *
 *	@params: ResponseWriter, Request, Name -> For execution and show the named page
 */
func ShowPage(w http.ResponseWriter, r *http.Request, name string){
	/*
	 *  Get template data
	 */
	// Get Userdata
	var data = UserData{models.UserMessage, models.GetUserData(r)}
	// Categories
	var categories = models.GetCategoriesFromBookmarks(data.User.Id)
	// Get Filter and Sorting
	var filter = r.FormValue("filter") // Set GET filter
	var sorting = r.FormValue("sort")     // Set GET Sort
	// Get Bookmarks
	var bookmarks []models.Bookmark
	if filter != "" || sorting != "" {
		// If Set process them
		bookmarks = models.GetBookmarksCustomisation(data.User.Id, filter, sorting)
	}else{
		bookmarks = models.GetBookmarks(data.User.Id)
	}
	// Category sort
	if sorting != "" {
		buffer := strings.Split(sorting, "_")
		sortValue := buffer[0]
		if sortValue == "ManuelCategories"{
			sort.Strings(categories)
			if buffer[1] == "DESC" {
				reverse(categories)
			}
		}
	}
	/*
	 *  Call template file
	 */
	// Set path
	r.URL.Path = "/" + name
	// Load templates
	t := template.Must(template.ParseFiles("./views/header.html",
		"./views/"+name+".html", "./views/footer.html"))
	// Executes templates with data (Message, Userdata, ...)
	t.ExecuteTemplate(w, "header", data)
	t.ExecuteTemplate(w, "content", BookmarkData{bookmarks, categories, data.User})
	t.ExecuteTemplate(w, "footer", data)
}

/*
 *	Basic function to handle the cookie check (logged in) and show all called pages with specific path
 *  All routes and calls (without ajax req and first call) should flow through this func
 *
 *	@params: ResponseWriter, Request -> For execution
 */
func InterfaceHandler(w http.ResponseWriter, r *http.Request){
	// DB
	db.OpenConnection()
	// Check cookies
	if login.CheckCookie(w, r) {
		if login.Message != "" {
			models.UserMessage = login.Message
		}
		// Show pages by URL path
		var path = r.URL.Path
		switch (path) {
			case "/profile":
				ShowPage(w, r, "profile")
				break
			case "/bookmarks":
				if !(checkForAjaxUpdates(w,r)) {
					// Else show page
					ShowPage(w, r, "bookmarks")
				}
				break
			case "/categories":
				ShowPage(w, r, "categories")
				break
			case "/impressum":
				ShowPage(w, r, "impressum")
				break
			case "/about":
				ShowPage(w, r, "about")
				break
			case "/policy":
				ShowPage(w, r, "policy")
				break
			case "/login":
				ShowLogin(w, r)
				break
			default:
				ShowPage(w, r, LANDING_PAGE)
				break
		}
	}else{
		// Redirect to login if no valid cookie found
		models.UserMessage = login.Message
		r.URL.Path = "/login"
		http.Redirect(w, r, r.Header.Get("Host") + "/login", http.StatusSeeOther)
	}
	db.CloseConnection()
}

/*
 *	Check GET requests
 *
 *	@params: ResponseWriter, Request -> For execution
 */
func checkForAjaxUpdates(w http.ResponseWriter, r *http.Request) bool{
	isSend := false
	// "Route" ajax updates
	count := r.FormValue("count")
	del := r.FormValue("del")
	update := r.FormValue("update")
	if count != "" {
		models.CountBookmarkClicks(count)
		isSend = true
	}else if del != "" {
		models.DelBookmark(bson.ObjectIdHex(del))
		isSend = true
	}else if update != "" {
		models.UpdateBookmarkValue(
			update,
			r.FormValue("id"),
			r.FormValue("content"),
			models.GetUserData(r).Id)
		isSend = true
	}
	return isSend
}

// Sort helper
func reverse(ss []string) {
	last := len(ss) - 1
	for i := 0; i < len(ss)/2; i++ {
		ss[i], ss[last-i] = ss[last-i], ss[i]
	}
}