/*
Goroutines
In Go, concurrency is achieved by using Goroutines. Goroutines are functions or methods which can run concurrently with others methods and functions. They are very much similar like threads in Java but light weight and cost of creating them is very low.

Advantages of Goroutines over threads are:
- Goroutines have a faster startup time than threads.
- Goroutines come with built-in primitives to communicate safely between themselves called as channels(We will come to it later).
- Goroutines are extremely cheap when compared to threads. They are only a few kb in stack size and the stack can grow and shrink according to needs of the application whereas in the case of threads the stack size has to be specified and is fixed.
*/
package controllers

import (
	"drueckMich/db"
	"drueckMich/models"
	"drueckMich/web"
	"drueckMich/wvr"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"io"
	"log"
	"net/http"
)

/*
 *	Collect all additional Bookmark data by load them from internet
 */
func getAdditionalBookmarkData(bmid bson.ObjectId){
	bm := models.GetBookmarkById(bmid) // Get bm id
	out := make(chan web.ScraperData)  // Channel
	out2 := make(chan []string) 	   // Channel
	// Goroutine with channels
	// This can take some time on big sites with much images
	// That's why this is also run in thread
	go func() { // synchronized thread with channels
		fmt.Print("Start web scrapper...\n")
		out <- web.Scrap(bm.Address)   			// DOM analysis for img files
	}()
	data := <-out // Sync data
	writeBookmarkDataIfReady(bm, data)
	// Get watson api key from config
	var apiKey = LoadConfiguration(configFile).WVR.Key
	go func(key string) { // synchronized thread with channels
		fmt.Print("Start watson analysis...\n")
		out2 <- wvr.CheckImages(data.Images, key)   	// Watson analysis for images
	}(apiKey)
	data2 := <-out2
	writeBookmarkCategorieIfReady(bm,data2)
}

/*
 *	Load and write additional bookmark-data to db
 *  Title -> From DOM title tag
 *  Description -> From DOM meta tag
 *  Icon -> From DOM link tag
 */
func writeBookmarkDataIfReady(bm models.Bookmark, data web.ScraperData){
	bm = models.GetBookmarkById(bm.Id) // Retake bm
	bm.Title = data.Title
	if len(data.MetaDesc) > 0 {
		bm.Description = data.MetaDesc[0]
	}
	// Check for valid mime type
	if GetMimeType(data.Icon) == "text/html" {
		data.Icon = ""
		bm.Icon = data.Icon
	}else{
		bm.Icon = "img_" + bm.Id.Hex() // Imagename as id with tag
	}
	models.UpdateBookmark(bm, bm.UserId)
	if data.Icon != "" {
		getIcon(data.Icon, bm.Icon)    // Load image to GridFS
	}
	fmt.Print("Job done...\n")
}

/*
 *	Get and write Icon to gridfs
 */
func getIcon(data string, name string){
	response, e := http.Get(data)
	if e != nil {
		log.Fatal(e)
	}
	//dec := base64.NewDecoder(base64.StdEncoding, response.Body)// dec is an io.Reader
	database := db.OpenCon()
	gridfs := database.GridFS(models.GRIDFS_COL_NAME)
	gridFile, err := gridfs.Create(name)
	if err != nil {
		log.Fatal(e)
	}
	_, err = io.Copy(gridFile, response.Body)
	if err != nil {
		log.Fatal(e)
	}
	defer response.Body.Close()
	err = gridFile.Close()
	if err != nil {
		log.Fatal(e)
	}
	defer db.CloseCon()
}

/*
 *	Write Categories
 */
func writeBookmarkCategorieIfReady(bm models.Bookmark, data []string){
	bm = models.GetBookmarkById(bm.Id) // Retake bm
	if len(data) >= 1{
		bm.AutoCategories = data[0]	   // Take first -> Next software version take all
		                               // For multiple Categories
		models.UpdateBookmark(bm, bm.UserId)
	}
	fmt.Print("Job done...\n")
}