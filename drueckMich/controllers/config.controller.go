package controllers

import (
	"encoding/json"
	"fmt"
	"os"
)

/*
 *	Initialisation
 */
var configs Config
var configFile = "conf/config.json"
// An Config represents the data from the config file
type Config struct {
	Database struct {
		Host     string `json:"host"`
		Name	 string `json:"name"`
		Grid	 string `json:"gridfs"`
		Port	 string `json:"port"`
	} `json:"db"`
	Web struct {
		Host     string `json:"host"`
		Port	 string `json:"port"`
	} `json:"web"`
	WVR struct {
		Key 		string	`json:"apiKey"`
	} `json:"wvr"`
	Extension struct {
		Name 		string	`json:"name"`
		ValueIn 	string	`json:"valueIn"`
	} `json:"extension"`
}

/*
 *	Loading the configuration file in a obj variable
 *
 *	@params: String -> Config-file path
 *	@return: Config -> New configurations
 */
func LoadConfiguration(file string) Config {
	if file == ""{
		file = configFile
	}
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		fmt.Println(err.Error())
		fmt.Println("Can't find config file")
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&configs)
	return configs
}

/*
 *	Returns configuration data
 */
func GetConfig() Config{
	return configs
}