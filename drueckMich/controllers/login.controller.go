// Class for handling login actions
// This class check und serialize actions for the login
// Package db, login, models is needed
package controllers

import (
	"drueckMich/db"
	"drueckMich/login"
	"drueckMich/models"
	"html/template"
	"net/http"
)

/*
*	Function to show the login
*
*	@params: ResponseWriter, Request -> For execution
*/
func ShowLogin(w http.ResponseWriter, r *http.Request){
	r.URL.Path = "/login"
	var data = UserData{models.UserMessage, models.GetUserData(r)}
	// Load templates
	t := template.Must(template.ParseFiles("./views/header.html",
		"./views/login.html", "./views/footer.html"))
	// Executes templates with data (Nav, Message)
	t.ExecuteTemplate(w, "header", data)
	t.ExecuteTemplate(w, "content", data)
	t.ExecuteTemplate(w, "footer", data)
}

/*
 *	Basic function to handle the login check
 *  First route and all login/registration request should flow through this
 *  This calling the login class to handle the complete login process
 *
 *	@params: ResponseWriter, Request -> For execution
 */
func CheckLogin(w http.ResponseWriter, r *http.Request) {
	// DB
	db.OpenConnection()
	// Check cookies or handle login process (login/registration) from class login
	if login.CheckLogin(w,r) {
		// Redirect to UI
		models.UserMessage = login.Message
		//r.URL.Path = "/"+LANDING_PAGE
		http.Redirect(w, r, r.Header.Get("Host") + "/" + LANDING_PAGE, http.StatusSeeOther)
	} else {
		// Show Login
		models.UserMessage = login.Message
		r.URL.Path = "/login"
		http.Redirect(w, r, r.Header.Get("Host") + "/login", http.StatusSeeOther)
	}
	db.CloseConnection()
}