package models

import "gopkg.in/mgo.v2/bson"

/*
 *	Function to get categories from all bookmarks
 *
 *	@params: bson.ObjectId -> User id
 *	@return: []string -> Category array
 */
func GetCategoriesFromBookmarks(uid bson.ObjectId) []string{
	var categories []string
	var bookamrks = GetBookmarks(uid)
	for _, bm := range bookamrks{
		if !(contains(categories, bm.AutoCategories) || contains(categories, bm.ManuelCategories)) {
			if bm.AutoCategories != "" {
				categories = append(categories, bm.AutoCategories)
			}
			if bm.ManuelCategories != "" {
				categories = append(categories, bm.ManuelCategories)
			}
		}
	}
	return categories
}

/*
*	Helper function contains
*/
func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
