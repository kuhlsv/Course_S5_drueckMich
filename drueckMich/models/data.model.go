// Class for data manipulation
// This class processing data of items (Add/Edit/Delete/Get)
// Package db is needed
package models

import (
	"drueckMich/db"
	"encoding/xml"
	"fmt"
	"golang.org/x/net/html"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

/*
 *	Initialisation
 */
const TMP_PATH = "./views/tmp/"
const XML_FILE_NAME = "drueckMichDB.xml"
const XML_FILE_NAME_BOOKMARKS = "bookmarks.xml"
const IMPORT_EXPORT_BTN = "dataFile"
const GRIDFS_COL_NAME = "Images"
var data []xmlExternalData

// Contains information to export/import the whole database
type xmlExportData struct {
	Bookmarks   []Bookmark			 `xml:"bookmarks"`
	Users      	[]UserData           `xml:"users"`
}

// Grid image item
type GridIcon struct {
	Content    string		 `bson:"content"`
	Name	   string		 `bson:"filename"`
	Length	   int64		 `bson:"length"`
}

/*
 * External Bookmark import structs
 */
// Contains external bookmark data for import
type xmlExternalData struct {
	Title    string  `xml:"DT>A"`
	Href     string  `xml:"HREF,attr"`
	Date	 string	 `xml:"ADD_DATE,attr"`
	Category string	 `xml:"H3"`
	Icon 	 string	 `xml:"ICON,attr"`
}

/*
 *	Function to import a XML-DB-Export-File to DB
 *
 *	@params: ResponseWriter, Request, Name -> For execution with ParseMultipartForm
 */
func Import(w http.ResponseWriter, r *http.Request) {
	// Get
	r.ParseMultipartForm(32 << 20)
	file, handler, err := r.FormFile(IMPORT_EXPORT_BTN)
	if err != nil {
		log.Print(err)
		return
	}
	f, err := os.Create(TMP_PATH + handler.Filename)
	// Close
	if err != nil {
		log.Print(err)
		return
	}
	// Write and read (rename)
	io.Copy(f, file)
	f.Close()
	file.Close()
	newPath := TMP_PATH + XML_FILE_NAME
	os.Rename(TMP_PATH+ handler.Filename, newPath)
	bytes, _ := ioutil.ReadFile(newPath)
	// Parse
	data := xmlExportData{}
	err = xml.Unmarshal(bytes, &data)
	if err != nil {
		log.Print(err)
		return
	}
	// DB
	database := db.OpenConnection()
	// Insert
	database.C(DB_COLL_BOOKMARKS).DropCollection() // Reset bookmarks
	for _, e := range data.Bookmarks {
		database.C(DB_COLL_BOOKMARKS).Insert(e)
	}
	database.C(DB_COLL_USER).DropCollection() // Reset users
	for _, e := range data.Users {
		database.C(DB_COLL_USER).Insert(e)
	}
	// Remove tmp file
	os.Remove(newPath) // maybe disable
}

/*
 *	Function to export the DB to a XML-File
 *
 *	@params: ResponseWriter, Request, Name -> For execution with xml for download
 */
func Export(w http.ResponseWriter, r *http.Request) {
	// Create
	data := xmlExportData{}
	data.Users = GetAllUserData()
	data.Bookmarks = GetAllBookmarks()
	// Define
	w.Header().Set("Content-Disposition", "attachment; filename=" +XML_FILE_NAME)
	w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Add("Access-Control-Expose-Headers", "Content-Disposition")
	// Parse and send
	xmlData, _ := xml.Marshal(data)
	io.Copy(w, strings.NewReader(string(xmlData)))
}

/*
 *	Function to get Icon by name
 *
 *	@params: bson.ObjectId -> User id
 *	@return: name -> Name of file
 */
func GetGridIconByName(name string) *mgo.GridFile{
	database := db.OpenCon()
	gridfs := database.GridFS(GRIDFS_COL_NAME)
	gridFile, err := gridfs.Open(name)
	if err != nil {
		log.Print("Can not get Image!")
	}
	return gridFile
}

/*
 *	Function to upload Bookmarks from external services to db
 *
 *	@params: ResponseWriter, Request -> Bookmarks by ParseMultipartForm
 */
func UploadBookmarks(w http.ResponseWriter, r *http.Request) {
	// Get
	r.ParseMultipartForm(32 << 20)
	file, handler, err := r.FormFile(IMPORT_EXPORT_BTN)
	if err != nil {
		log.Print(err)
		file.Close()
		return
	}
	f, err := os.Create(TMP_PATH + handler.Filename)
	// Close
	if err != nil {
		log.Print(err)
		f.Close()
		file.Close()
		return
	}
	// Write and read (rename)
	bytes, err := ioutil.ReadAll(file)
	_, err = io.Copy(f, file)
	if err != nil {
		log.Print(err)
		file.Close()
		f.Close()
		return
	}
	f.Close()
	file.Close()
	newPath := TMP_PATH + XML_FILE_NAME_BOOKMARKS
	os.Rename(TMP_PATH + handler.Filename, newPath)
	//bytes, err := ioutil.ReadFile(newPath)
	if err != nil {
		log.Print(err)
		return
	}
	// Parse
	var replacer = strings.NewReplacer("<DL>", "", "<DT>", "", "</DL>", "", "</DT>", "", "<p>", "")
	str := replacer.Replace(strings.Split(string(bytes),"</H1>")[1])
	str = "<body>" + str + "</body>"
	docZeiger, err := html.Parse(strings.NewReader(str))
	searchForAItems(docZeiger, "")
	if err != nil {
		log.Fatal(err)
	}
	// Insert
	log.Print("Add Bookmarks from Import...")
	uid := GetUserData(r).Id
	for _, d := range data {
		bm := Bookmark{
			bson.NewObjectId(),
			uid,
			d.Href,
			d.Title,
			"",
			0,
			0,
			d.Category,
			"",
			"",
			0,
			"",
		}
		AddBookmark(bm)
		log.Print(bm.Title)
	}
	UserMessage = "Imported Bookmarks!"
	log.Print("Done")
	// Remove tmp file
	os.Remove(newPath)
	data = nil
}
/*
 *	Search recursive for descriptions and write into the map
 *
 *	@params: Node -> The next html node
 */
func searchForAItems(node *html.Node, category string) {
	if node.Type == html.ElementNode && node.Data == "a" {
		bm := xmlExternalData{}
		for _, name := range node.Attr {
			if name.Key == "href" {
				bm.Href = name.Val
			}
			if name.Key == "add_date" {
				bm.Date = name.Val
			}
			if category != "" {
				bm.Category = category
			}
			//if name.Key == "ICON" {
			//	bm.Icon = name.Val
			//}
		}
		bm.Title = node.FirstChild.Data
		fmt.Print(bm.Category)
		data = append(data, bm)
	}
	for child := node.FirstChild; child != nil; child = child.NextSibling {
		if child.Type == html.ElementNode && child.Data == "h3"{
			category = child.FirstChild.Data
		}
		searchForAItems(child, category)
	}
}
