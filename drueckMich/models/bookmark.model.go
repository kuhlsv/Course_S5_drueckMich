package models

import (
	"drueckMich/db"
	"drueckMich/helpers"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"log"
	"strings"
	"time"
)

/*
 *	Initialisation
 */
const DB_COLL_BOOKMARKS = "Bookmarks"
const DATE_LAYOUT = "02.01.2006"

// A Bookmark represents a bookmark with additional data
type Bookmark struct{
	Id               bson.ObjectId   `bson:"_id"`
	UserId           bson.ObjectId   `bson:"UserId"`
	Address          string          `bson:"Address"`
	Title            string          `bson:"Title"`
	Description      string          `bson:"Description"`
	Long			 int			 `bson:"Longitude"`
	Lat			 	 int			 `bson:"Latitude"`
	ManuelCategories string  		 `bson:"ManuelCategories"`
	AutoCategories	 string			 `bson:"AutoCategories"`
	Date             string	       	 `bson:"Date"`
	Clicks           int             `bson:"Clicks"`
	Icon             string			 `bson:"Icon"`
}

/*
 *	Function to build a new bookmark by combine all information
 *
 *	@params: bson.ObjectId, String -> User and the link from the extension or user input
 *  @return: bson.ObjectId -> The id of the new Bookmark
 */
func BuildBookmark(uid bson.ObjectId, link string) bson.ObjectId{
	var isDoubledId bson.ObjectId
	for _, currentBm := range GetBookmarks(uid){
		if currentBm.Address == link{
			isDoubledId = currentBm.Id
		}
	}
	if isDoubledId == "" {
		bm := Bookmark{
			bson.NewObjectId(),
			uid,
			link,
			"",
			"",
			0,
			0,
			"",
			"",
			GetTime(),
			0,
			"",
		}
		isDoubledId = bm.Id
		AddBookmark(bm)
	}else{
		UserMessage = "Already bookmarked!"
	}
	return isDoubledId
}

/*
 *	Function to add an new bookmark to the db
 *
 *	@params: Bookmark -> Bookmark data
 */
func AddBookmark(bm Bookmark){
	database := db.OpenConnection()
	coll := database.C(DB_COLL_BOOKMARKS)
	bm2 := Bookmark{}
	coll.Find(bson.M{"Title" : bm.Title}).One(&bm2)
	// Check not present
	if len(bm2.Title) == 0 {
		// Write to db
		coll.Insert(bm)
	} else {
		UserMessage = "Error: Bookmark already exist"
	}
	db.CloseConnection()
}

/*
 *	Function to write updates of a Bookmark to the db
 *
 *	@params: bson.ObjectId, bson.ObjectId -> Bookmark id and the User id
 */
func UpdateBookmark(newBm Bookmark, uid bson.ObjectId){
	database := db.OpenConnection()
	coll := database.C(DB_COLL_BOOKMARKS)
	oldBm := Bookmark{}
	coll.Find(bson.M{"_id" : newBm.Id, "UserId" : uid}).One(&oldBm)
	if oldBm.UserId == uid{
		// Write to db
		bm := Bookmark{
			oldBm.Id,
			oldBm.UserId,
			oldBm.Address,
			helpers.IfThenElseString(newBm.Title == "", oldBm.Title, newBm.Title),
			helpers.IfThenElseString(newBm.Description == "", oldBm.Description, newBm.Description),
			helpers.IfThenElseInt(newBm.Long == 0, oldBm.Long, newBm.Long),
			helpers.IfThenElseInt(newBm.Lat == 0, oldBm.Lat, newBm.Lat),
			helpers.IfThenElseString(newBm.ManuelCategories == "", oldBm.ManuelCategories, newBm.ManuelCategories),
			helpers.IfThenElseString(newBm.AutoCategories == "", oldBm.AutoCategories, newBm.AutoCategories),
			oldBm.Date,
			helpers.IfThenElseInt(newBm.Clicks > 0, newBm.Clicks, oldBm.Clicks),
			helpers.IfThenElseString(newBm.Icon == "", oldBm.Icon, newBm.Icon),
		}
		coll.Update(oldBm, bm)
	}
	db.CloseConnection()
}

/*
 *	Function to write specific value updates of a Bookmark to the db
 *
 *	@params: bson.ObjectId, bson.ObjectId -> Bookmark id and the User id
 */
func UpdateBookmarkValue(item string, id string, content string, uid bson.ObjectId){
	database := db.OpenConnection()
	coll := database.C(DB_COLL_BOOKMARKS)
	oldBm := Bookmark{}
	newBm := Bookmark{}
	err := coll.Find(bson.M{"_id" : bson.ObjectIdHex(id), "UserId" : uid}).One(&oldBm)
	if err != nil {
		log.Print(err)
	}
	if oldBm.UserId == uid{
		newBm = oldBm
		// Write to db
		switch item{
		case "title":
			newBm.Title = content
			break
		case "description":
			newBm.Description = content
			break
		case "categories":
			newBm.ManuelCategories = content
			break
		default:
			break
		}
		err := coll.Update(oldBm, newBm)
		if err != nil {
			log.Print(err)
		}
	}
	db.CloseConnection()
}

/*
 *	Function to increment bookmark clicks by get request
 *
 *	@params: string -> BM id as string
 */
func CountBookmarkClicks(id string) {
	bmid := bson.ObjectIdHex(id)
	bm := GetBookmarkById(bmid)
	bm.Clicks = bm.Clicks + 1
	UpdateBookmark(bm, bm.UserId)
}

/*
 *	Function to delete a Bookmark from db
 *
 *	@params: bson.ObjectId -> Bookmark id
 */
func DelBookmark(bmid bson.ObjectId){
	database := db.OpenConnection()
	coll := database.C(DB_COLL_BOOKMARKS)
	coll.Remove(bson.M{"_id" : bmid})
	db.CloseConnection()
}

/*
 *	Function to get Bookmarks from a user
 *
 *	@params: bson.ObjectId -> User id
 *	@return: []Bookmark -> Struct with arrays of Bookmark
 */
func GetBookmarks(uid bson.ObjectId) []Bookmark{
	result := []Bookmark{}
	database := db.OpenConnection()
	coll := database.C(DB_COLL_BOOKMARKS)
	coll.Find(bson.M{"UserId" : uid}).All(&result)
	db.CloseConnection()
	return result
}

/*
 *	Function to get a Bookmark
 *
 *	@params: bson.ObjectId -> Bookmark id
 *	@return: Bookmark -> Struct of Bookmark
 */
func GetBookmarkById(bmid bson.ObjectId) Bookmark{
	result := Bookmark{}
	database := db.OpenConnection()
	coll := database.C(DB_COLL_BOOKMARKS)
	coll.Find(bson.M{"_id" : bmid}).One(&result)
	db.CloseConnection()
	return result
}


/*
 *	Function to get all Bookmarks
 *
 *	@return: []Bookmark -> Struct with arrays of Bookmark
 */
func GetAllBookmarks() []Bookmark{
	result := []Bookmark{}
	database := db.OpenConnection()
	coll := database.C(DB_COLL_BOOKMARKS)
	coll.Find(nil).All(&result)
	db.CloseConnection()
	return result
}

/*
 *	Function for uniform date/time handling
 *  Using UTC -> Clients have to convert time to zone
 *
 *	@return: String -> Current date as string
 */
func GetTime() string{
	t := time.Now().UTC().Format(DATE_LAYOUT)
	if t == "" {
		t = time.Now().String()
	}
	return t
}

/*
 *	Function to get Bookmarks from a user with filters or sorting
 *
 *	@params: bson.ObjectId -> User id
 *	@return: []Bookmark -> Struct with arrays of Bookmark
 */
func GetBookmarksCustomisation(uid bson.ObjectId, filter string, sort string) []Bookmark{
	filterValue := ""
	sortValue := ""
	sortDirection := ""
	if sort != "" {
		buffer := strings.Split(sort, "_")
		sortDirection = helpers.IfThenElseString(buffer[1] == "DESC","-","")
		sortValue = buffer[0]
	}else{
		sortValue = "Date"
	}
	if filter != "" {
		filterValue = filter
		fmt.Print("Filter for: "+filterValue)
	}
	result := []Bookmark{}
	database := db.OpenConnection()
	coll := database.C(DB_COLL_BOOKMARKS)
	var condition = bson.M{}
	if (filterValue == ""){
		condition = bson.M{"UserId": uid}
	}else{
		condition = bson.M{"UserId": uid, "$or": []bson.M{
			bson.M{"Title": bson.RegEx{filterValue, "i"}},
			bson.M{"Description": bson.RegEx{filterValue, "i"}},
			bson.M{"ManuelCategories": bson.RegEx{filterValue, "i"}},
			bson.M{"AutoCategories": bson.RegEx{filterValue, "i"}},
		},}
	}
	// Process Filter and Sort
	err := coll.Find(condition).
		Sort(sortDirection+sortValue).
		All(&result)
	if err != nil {
		fmt.Print(err)
		coll.Find(bson.M{"UserId": uid}).All(&result)
	}
	db.CloseConnection()
	return result
}