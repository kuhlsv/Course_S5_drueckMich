// Class for user manipulation
// This class processing data of users (Set data/Delete/Get)
// Package db, login is needed
package models

import (
	"drueckMich/db"
	"drueckMich/login"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"
)

/*
 *	Initialisation
 */
const COOKIE_NAME = "session"
const DB_COLL_USER = "Userdata"
const AVATAR_PATH = "./views/images/avatar/"
const AVATAR_NAME_ADDITIVE = "avatar_"
const AVATAR_BTN = "avatarFile"
// The message for to user-interface in a popup dialog
var UserMessage = ""

// An UserData represents a user with additional data
type UserData struct{
	Id 	 bson.ObjectId 	`bson:"_id"`
	Username string 	`bson:"Username"`
	Title string 		`bson:"Title"`
	Password string 	`bson:"Password"`
	SessionId string 	`bson:"Session"`
	Theme int 			`bson:"Theme"`
	Avatar string 		`bson:"Avatar"`
}

/*
 *	Function to get all data from the user by session
 *
 *	@params: Session(String) -> Get data from specific user
 *	@return: UserData(type from model) -> Struct with data of the user
 */
func GetUserBySession(session string) UserData {
	result := UserData{}
	if session != "" {
		database := db.OpenConnection()
		coll := database.C(DB_COLL_USER)
		coll.Find(bson.M{"Session": session}).One(&result)
		db.CloseConnection()
	}
	return result
}

/*
 *	Function to get all data from User by username
 *
 *	@params: Username(String) -> Get data from specific user
 *	@return: UserData(type from model) -> Struct with data of the user
 */
func GetUserByUname(uname string) UserData {
	result := UserData{}
	if uname != "" {
		database := db.OpenConnection()
		coll := database.C(DB_COLL_USER)
		coll.Find(bson.M{"Username": uname}).One(&result)
		db.CloseConnection()
	}
	return result
}

/*
 *	Function to set a theme color
 *
 *	@params: Request, Title(String) -> Set theme integer
 */
func SetTheme(r *http.Request, title string) {
	i, err := strconv.Atoi(title)
	if err == nil {
		user := GetUserData(r)
		database := db.OpenConnection()
		coll := database.C(DB_COLL_USER)
		coll.Update(user, bson.M{"$set": bson.M{"Theme": i}})
	}
	db.CloseConnection()
}

/*
 *	Function to set a title(like admin/developer) to an user
 *
 *	@params: Request, Title(String) -> Set title to user by session
 */
func SetTitle(r *http.Request, title string){
	user := GetUserData(r)
	database := db.OpenConnection()
	coll := database.C(DB_COLL_USER)
	coll.Update(user, bson.M{"$set": bson.M{ "Title" : title }})
	db.CloseConnection()
}

/*
 *	Function to update username from an user
 *
 *	@params: Request, Username(String) -> Update username by session
 */
func UpdateUsername(r *http.Request, uname string){
	// Check username already used
	if len(GetUserByUname(uname).Username) == 0 {
		// Get user by session
		user := GetUserData(r)
		database := db.OpenConnection()
		coll := database.C(DB_COLL_USER)
		if len(user.Id.String()) > 1 {
			coll.Update(user, bson.M{"$set": bson.M{ "Username" : uname }})
		}
		db.CloseConnection()
	}else{
		UserMessage = "Username already in user!"
	}
}

/*
 *	Function to a avatar image to an user
 *
 *	@params: Request, Path(String) -> Set avatar to user by session into db
 */
func SetAvatar(r *http.Request, path string){
	user := GetUserData(r)
	database := db.OpenConnection()
	coll := database.C(DB_COLL_USER)
	coll.Update(user, bson.M{"$set": bson.M{ "Avatar" : path }})
	db.CloseConnection()
}

/*
 *	Function to delete the user
 *
 *	@params: ResponseWriter, Request -> For execute and get the user by session
 */
func DeleteUser(w http.ResponseWriter, r *http.Request){
	database := db.OpenConnection()
	coll := database.C(DB_COLL_USER)
	user := GetUserData(r)
	coll.Remove(bson.M{ "Session" : user.SessionId })
	// Logout
	db.CloseConnection()
	login.DeleteCookie(w)
	// Delete avatar and events/relations
	DeleteAvatar(user.Avatar)
	//DelEventsById(user.Id)
}

/*
 *	Function to get all data from current User (Session)
 *
 *	@params: Request -> Get data from user by session
 *	@return: UserData(type from model) -> Struct with data of the user
 */
func GetUserData(r *http.Request) UserData{
	cookie, _ := r.Cookie(COOKIE_NAME)
	data := UserData{}
	if cookie != nil {
		data = GetUserBySession(cookie.Value)
	}
	return data
}

/*
 *	Function to get all data from User
 *
 *	@return: []UserData(type from model) -> Struct arrays with data of the users
 */
func GetAllUserData() []UserData{
	data := []UserData{}
	database := db.OpenConnection()
	coll := database.C(DB_COLL_USER)
	coll.Find(nil).All(&data)
	db.CloseConnection()
	return data
}

/*
 *	Function to upload an avatar image and write to image files
 *
 *	@params: ResponseWriter, Request -> Image by ParseMultipartForm
 */
func UploadAvatar(w http.ResponseWriter, r *http.Request) {
	// Get
	r.ParseMultipartForm(32 << 20)
	file, handler, err := r.FormFile(AVATAR_BTN)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()
	user := GetUserData(r)
	f, err := os.Create(AVATAR_PATH + handler.Filename)
	// Close
	if err != nil {
		fmt.Println(err)
		return
	}
	io.Copy(f, file)
	f.Close()
	// Rename
	ending := strings.SplitAfter(handler.Filename, ".")
	newPath := AVATAR_PATH + AVATAR_NAME_ADDITIVE + user.Username + "." + ending[len(ending)-1]
	newPathHTML := AVATAR_NAME_ADDITIVE + user.Username + "." + ending[len(ending)-1]
	os.Rename(AVATAR_PATH+ handler.Filename, newPath)
	// Update User
	SetAvatar(r, newPathHTML)
}

/*
 *	Function to delete an avatar image TODO
 *
 *	@params: Path(String) -> Path to file
 */
func DeleteAvatar(path string) {
	f, err := os.Create(AVATAR_PATH + path)
	f.Close()
	// Close
	if err != nil {
		fmt.Println(err)
		return
	}else{
		err = os.Remove(AVATAR_PATH + path)
		if err != nil{
			UserMessage = "Error: " + err.Error()
			return
		}
	}
}